const WIDTH = document.body.clientWidth;
const HEIGHT = document.body.clientHeight;

//定义 scene, camera, render
let scene = new THREE.Scene();
//透视投影
let camera = new THREE.PerspectiveCamera(75, WIDTH/HEIGHT, 0.1, 2000);
//正射投影
//camera = new THREE.OrthographicCamera( WIDTH / -2, WIDTH / 2, HEIGHT / 2, HEIGHT / -2, 1, 2000 );
camera.position.z = 150;
//antialias 抗锯齿
let renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setSize(WIDTH, HEIGHT);

document.querySelector('#container').appendChild(renderer.domElement);


//地球半径
const R_EARTH = 20;
//月球半径
const R_MOON = 10;
//月球绕地半径
const R_MOON_ORBIT = 2210;
//卫星绕地半径
const R_SATELLITE_ORBIT = 110;

//定义 地球, 月球, 卫星
//地球还包含云层
let earth = new THREE.Group();
let moon = null;
let satellite = null;

/**
 * 渲染
 */
function animateRunner(){
	this.delta = 0;
	this.run = function(){
		this.delta += 0.01;
		if( moon ){
			moon.position.x = R_MOON_ORBIT * Math.cos(this.delta);
			moon.position.y = R_MOON_ORBIT * Math.sin(this.delta);
			moon.position.z = 0;
			moon.rotation.x -= 0.009;
		}
		if( satellite ){
			satellite.position.x = R_SATELLITE_ORBIT * Math.cos(this.delta * 2);
			satellite.position.y = R_SATELLITE_ORBIT * Math.sin(this.delta * 2);
			satellite.position.z = 0;
		}

		earth.rotation.y -= 0.002;

		requestAnimationFrame( this.run.bind(this) );
		renderer.render( scene, camera );
	}
}
/**
 * 导入纹理
 * @param path
 * @returns {Promise}
 */
function loadTexture(path){
	return new Promise((resolve, reject)=>{
		let loader = new THREE.TextureLoader();
		loader.load(path, texture => {
			resolve(texture);
		} , ()=>{} , ()=>{reject('fail')});
	});
}

async function init(){
	//地球本体
	earth.position.z = 0;

	let earthTexture = await loadTexture('img/earth.jpg');
    let earthGeometry = new THREE.SphereGeometry( R_EARTH, 50, 50 );
    let earthMaterial = new THREE.MeshBasicMaterial( { map: earthTexture, overdraw: 0.5 } );
    let earthMesh = new THREE.Mesh( earthGeometry, earthMaterial );
    earth.add( earthMesh );

	//地球的云层
	let cloudTexture = await loadTexture('img/clouds.png');
    let cloudGeometry = new THREE.SphereGeometry( R_EARTH+50, 50, 50 );
    let cloudMaterial = new THREE.MeshBasicMaterial( { map: cloudTexture, overdraw: 0.5, transparent: true, side: THREE.DoubleSide} );
    let cloudMesh = new THREE.Mesh( cloudGeometry, cloudMaterial );
    earth.add( cloudMesh );

	//月球
	let moonTexture = await loadTexture('img/moon.jpg');
    let moonGeometry = new THREE.SphereGeometry( R_MOON, 50, 50 );
    let moonMaterial = new THREE.MeshBasicMaterial( { map: moonTexture} );
    moon = new THREE.Mesh( moonGeometry, moonMaterial );

	//卫星
    let satelliteGeometry = new THREE.SphereGeometry( 2, 50, 50 );
    let satelliteMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
    satellite = new THREE.Mesh( satelliteGeometry, satelliteMaterial );

    scene.add(earth);
    scene.add(moon);
    scene.add(satellite);

    let animater = new animateRunner();
    animater.run();
}

//start main.js
init().catch(e=>console.log(e));